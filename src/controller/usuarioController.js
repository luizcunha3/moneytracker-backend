const { verificaUsuarioExiste, cadastraUsuario } = require('../model/usuario');
const logaEntradaUsuario = usuario => {
  return verificaUsuarioExiste(usuario)
  .then(res => {
    if(!res.isUsuarioCadastrado){
      return cadastraUsuario(usuario)
    } else {
      return { wasUsuarioInserido: false, message: 'usuario ja constava no banco', usuarioId: res.usuarioId }
    }
  })
  .then(responseCadstroUsuario => {
    return responseCadstroUsuario;
  })
  .catch(err => {
    console.log(err)
  })
}

module.exports = { logaEntradaUsuario }
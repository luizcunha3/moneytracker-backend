const { criaDespesaRecorrente, criaDespesa, saveDespesa, getDespesas, despesaFromDb } = require('../model/despesa');

const somaDespesas = (despesas) => {
    let despesasSomadas = []
    let categoriasSomadas = []
    despesas.map(val => {
        const currentCategoria = val.categoria.id;
        if (categoriasSomadas.indexOf(currentCategoria) === -1) {
            categoriasSomadas.push(currentCategoria);
            const soma = despesas.filter(obj => obj.categoria.id === currentCategoria).reduce((acc, val) => acc += val.valor, 0)
            despesasSomadas.push({ categoria: currentCategoria, valor: soma })
        }
    })
    return despesasSomadas;
}

const getDespesasPorUsuario = usuario => {
    return getDespesas(usuario)
        .then(result => {
            return result.map(row => {
                return despesaFromDb(row)
            })     
        })
        .catch(err => {
            console.log(err)
        })
    console.log('fidaputa')
}

const saveDespesaDB = despesa => {
    return new Promise((resolve, reject) => {
        const despesaCriada = criaDespesa(despesa)
        if (despesaCriada.erro) {
            reject(despesaCriada)
        } else {
            return saveDespesa(despesaCriada)
        }

    })

}

module.exports = { somaDespesas, getDespesasPorUsuario, saveDespesaDB }
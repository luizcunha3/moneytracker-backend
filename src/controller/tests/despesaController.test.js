const { somaDespesas } = require('../despesaController');
const despesasMockRandom = [
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        descricao: "LALALLA",
        valor: 12.58
    },
    {
        categoria: {
            nome: "Balada",
            id: "2"
        },
        descricao: "LALALLA",
        valor: 56.00
    },
    {
        categoria: {
            nome: "Besteira",
            id: "3"
        },
        descricao: "LALALLA",
        valor: 2.5
    },
    {
        categoria: {
            nome: "Contas",
            id: "4"
        },
        descricao: "LALALLA",
        valor: 210.00
    },
    {
        categoria: {
            nome: "Contas",
            id: "1"
        },
        descricao: "LALALLA",
        valor: 30.15
    },
    {
        categoria: {
            nome: "Besteira",
            id: "3"
        },
        descricao: "LALALLA",
        valor: 20.15
    },
]

const despesasMock20 = [
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
    {
        categoria: {
            nome: "Compras",
            id: "1"
        },
        valor: 10
    },
]

test('Soma despesas por categoria', (done) => {
    const objetoEsperado = [
        { categoria: "1", valor: 42.73 },
        { categoria: "2", valor: 56.00 },
        { categoria: "3", valor: 22.65 },
        { categoria: "4", valor: 210 }
    ]

    expect(somaDespesas(despesasMockRandom)).toEqual(objetoEsperado);
    done();
})

test('Soma muitas despesas com mesmo id', (done) => {
    const objetoEsperado = [
        { categoria: "1", valor: 200 },
    ]
    expect(somaDespesas(despesasMock20)).toEqual(objetoEsperado);
    done();
})
const { baseQuery } = require('../utils/db.utils')

const verificaUsuarioExiste = usuario => {
  const query = 'SELECT * FROM usuario WHERE ?'
  const params = { id_auth: usuario.uid }
  return baseQuery(query, params)
    .then(result => {
      if (result.length === 1) {
        return { isUsuarioCadastrado: true, message: 'usuario ja cadastrado', usuarioId: result[0].id }
      } else {
        return { isUsuarioCadastrado: false, message: 'usuario nao cadastrado' }
      }
    })
    .catch(err => {
      console.log(err)
    })
}

const cadastraUsuario = usuario => {
  const query = `INSERT INTO usuario SET ?`
  const params = {
    email: usuario.email,
    nome: usuario.displayName,
    id_auth: usuario.uid,
    photo_url: usuario.photoURL
  }
  return baseQuery(query, params)
    .then(res => {
      if(res.affectedRows === 1){
        return { wasUsuarioInserido: true, message: 'usuario inserido com sucesso', usuarioId: res.insertID } 
      } else {
        return { wasUsuarioInserido: false, message: 'usuario nao foi inserido' }
      }
    })
    .catch(err => {
      console.log(err)
    })
}

module.exports = { verificaUsuarioExiste, cadastraUsuario}
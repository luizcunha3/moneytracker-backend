const moment = require('moment')
const { criaDespesaRecorrente } = require('../despesa');
let mockEntrada = {
  despesa: {
    idCategoria: "1",
    nomeCategoria: "Compras",
    descricao: "LALALA",
    valor: 22.21,
    usuario: 1,
    data: {
      ano: 2018,
      mes: 6,
      dia: 5,
      hora: 16,
      minuto: 31,
      ms: 0
    }
  },
  vezes: 2
}

beforeEach(() => {
  mockEntrada = {
    despesa: {
      idCategoria: "1",
      nomeCategoria: "Compras",
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: {
        ano: 2018,
        mes: 6,
        dia: 5,
        hora: 16,
        minuto: 31,
        ms: 0
      }
    },
    vezes: 2
  }
})

test("Cria despesa recorrente de 2 meses", done => {
  const objetoEsperado = [
    {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(6).year(2018).hour(16).minute(31).millisecond(0) //05/07/2018 @ 4:31pm (UTC)

    },
    {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(7).year(2018).hour(16).minute(31).millisecond(0)
    }
  ]
  expect(criaDespesaRecorrente(mockEntrada.despesa, mockEntrada.vezes)).toEqual(objetoEsperado);
  done();
})

test("Cria despesa recorrente de 3 meses", done => {
  const objetoEsperado = [
    {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(6).year(2018).hour(16).minute(31).millisecond(0) //05/07/2018 @ 4:31pm (UTC)

    },
    {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(7).year(2018).hour(16).minute(31).millisecond(0)
    },
    {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(8).year(2018).hour(16).minute(31).millisecond(0)
    },
  ]
  expect(criaDespesaRecorrente(mockEntrada.despesa, 3)).toEqual(objetoEsperado);
  done();
})

test("Cria despesa recorrente de 7 meses", done => {

  const objetoEsperado = [
    {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(6).year(2018).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(7).year(2018).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(8).year(2018).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(9).year(2018).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(10).year(2018).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(11).year(2018).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(0).year(2019).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(1).year(2019).hour(16).minute(31).millisecond(0)
    }, {
      categoria: {
        id: "1",
        nome: "Compras"
      },
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: moment().date(5).month(2).year(2019).hour(16).minute(31).millisecond(0)
    },
  ]
  expect(criaDespesaRecorrente(mockEntrada.despesa, 9)).toEqual(objetoEsperado);
  done();
})
const { criaDespesa } = require('../despesa');
const moment = require('moment')
let mockEntrada = {
    idCategoria: "1",
    nomeCategoria: "Compras",
    descricao: "LALALA",
    valor: 22.21,
    usuario: 1,
    data: {
        ano: 2018,
        mes: 6,
        dia: 5,
        hora: 16,
        minuto: 31,
        ms: 0
    } 
}
beforeEach(() => {
    mockEntrada = {
        idCategoria: "1",
        nomeCategoria: "Compras",
        descricao: "LALALA",
        valor: 22.21,
        usuario: 1,
        data: {
            ano: 2018,
            mes: 6,
            dia: 5,
            hora: 16,
            minuto: 31,
            ms: 0
        }
    }
})
test('Testa a criação de um objeto de despesa', done => {
    const data = moment().date(5).month(6).year(2018).hour(16).minute(31).millisecond(0);
    const objetoEsperado = {
        categoria: {
            id: "1",
            nome: "Compras"
        },
        descricao: "LALALA",
        valor: 22.21,
        usuario: 1,
        data: data
        
    }
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado);
    done();
})

test('Testa criação de despesa sem idCategoria', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["idCategoria"]
    }
    delete mockEntrada.idCategoria
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})

test('Testa criação de despesa sem nomeCategoria', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["nomeCategoria"]
    }
    delete mockEntrada.nomeCategoria
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})

test('Testa criação de despesa sem descricao', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["descricao"]
    }
    delete mockEntrada.descricao
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})

test('Testa criação de despesa sem valor', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["valor"]
    }
    delete mockEntrada.valor
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})

test('Testa criação de despesa sem data', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["data"]
    }
    delete mockEntrada.data
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})

test('Testa criação de despesa sem valor e sem descrição', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["descricao", "valor"]
    }
    delete mockEntrada.valor
    delete mockEntrada.descricao
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})

test('Testa criação de despesa sem data e sem descrição', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["valor", "data"]
    }
    delete mockEntrada.valor
    delete mockEntrada.data
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})

test('Testa criação de despesa sem usuario e sem descrição', done => {
    const objetoEsperado = {
        erro: true,
        campos: ["usuario"]
    }
    delete mockEntrada.usuario
    expect(criaDespesa(mockEntrada)).toEqual(objetoEsperado)
    done();
})
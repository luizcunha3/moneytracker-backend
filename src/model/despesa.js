const moment = require('moment')
const { baseQuery } = require('../utils/db.utils')



const despesaFromDb = despesaDb => {
    return {
        categoria: {
            id: despesaDb.categoria_id,
            nome: despesaDb.nome_categoria,
        },
        usuario: despesaDb.usuario_id,
        descricao: despesaDb.descricao,
        valor: despesaDb.valor,
        data: despesaDb.data
    }
}

const saveDespesa = despesa => {
    const query = `INSERT INTO despesa SET ?`
    const params = {
        valor: parseFloat(despesa.valor),
        descricao: despesa.descricao,
        data_despesa: new Date(`${despesa.data.year()}-${despesa.data.month() + 1}-${despesa.data.date()} ${despesa.data.hour()}:${despesa.data.minute()}:00`),
        categoria_id: parseInt(despesa.categoria.id),
        usuario_id: parseInt(despesa.usuario)
    }
    return baseQuery(query, params);
}

const getDespesas = usuario => {
    const query = 'SELECT * FROM despesa WHERE ?'
    const params = {usuario_id: parseInt(usuario)}
    return baseQuery(query, params)
}

const criaDespesa = (despesa, dataDespesa = _despesaObjToMoment(despesa.data)) => {
    const resultadoValidacaoCampos = _validaCamposDespesa(despesa);
    if (resultadoValidacaoCampos.erro) return resultadoValidacaoCampos;
    return {
        categoria: {
            id: despesa.idCategoria,
            nome: despesa.nomeCategoria
        },
        usuario: despesa.usuario,
        descricao: despesa.descricao,
        valor: despesa.valor,
        data: dataDespesa
    }
}

const criaDespesaRecorrente = (despesa, recorrencia) => {
    let despesas = []

    for (i = 0; i < recorrencia; i++) {
        let data = _despesaObjToMoment(despesa.data).add(i, 'month');
        despesas.push(criaDespesa(despesa, data))
    }

    return despesas;
}


const _despesaObjToMoment = data => {
    try {
        return moment().date(data.dia).month(data.mes).year(data.ano).hour(data.hora).minute(data.minuto).millisecond(0);
    } catch (err) {

    }
}

const _validaCamposDespesa = obj => {
    res = { erro: false, campos: [] }
    obj.idCategoria ? null : res.campos.push("idCategoria")
    obj.nomeCategoria ? null : res.campos.push("nomeCategoria")
    obj.descricao ? null : res.campos.push("descricao")
    obj.valor ? null : res.campos.push("valor")
    obj.data ? null : res.campos.push("data")
    obj.usuario ? null : res.campos.push("usuario")
    res.erro = res.campos.length > 0
    return res;
}

module.exports = { criaDespesa, criaDespesaRecorrente, saveDespesa, getDespesas, despesaFromDb }
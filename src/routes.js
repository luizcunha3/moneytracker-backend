const { criaDespesaRecorrente, criaDespesa, saveDespesa, getDespesas } = require('./model/despesa');
const { saveDespesaDB, getDespesasPorUsuario } = require('./controller/despesaController')
const { logaEntradaUsuario } = require('./controller/usuarioController')
const routes = (server) => {

  server.get('/', (req, res) => {
    res.send(200, "backend running ok")
  })

  server.get('/lalalala', (req, res) => {
    const despesa = {
      idCategoria: "3",
      nomeCategoria: "Compras",
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: {
        ano: 2018,
        mes: 6,
        dia: 5,
        hora: 16,
        minuto: 31,
        ms: 0
      }
    }

    saveDespesaDB(despesa)
      .then(result => {
        res.send(200, "Tudo ok")
      })
      .catch(err => {
        res.send(200, err)
      })

  })

  server.get('/despesas/usuario/:usuario', (req, res) => {
    getDespesasPorUsuario(req.params.usuario).then(result => {
      res.send(200, result)
    })
  })

  server.get('/despesas/:times', (req, res) => {
    const despesa = {
      idCategoria: "1",
      nomeCategoria: "Compras",
      descricao: "LALALA",
      valor: 22.21,
      usuario: 1,
      data: {
        ano: 2018,
        mes: 6,
        dia: 5,
        hora: 16,
        minuto: 31,
        ms: 0
      }
    }
    res.send(200, criaDespesaRecorrente(despesa, req.params.times))
  })

  server.post('/login', (req, res, next) => {
    const { displayName, email, uid, photoURL } = req.body.user;
    const user = { displayName, email, uid, photoURL }
    logaEntradaUsuario(user).then(response => {
      user.idUsuario = response.usuarioId;
      user.usuarioNovo = response.wasUsuarioInserido;
      res.send(200, { usuario: user })
    })

  })
}

module.exports = routes;
const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware')

const { getConnection } = require('./src/utils/db.utils')
const server = restify.createServer();

const cors = corsMiddleware({
  preflightMaxAge: 5, //Optional
  origins: ['*'],
  allowHeaders: ['*'],
})

server.pre(cors.preflight)
server.use(cors.actual)

server.use(restify.plugins.bodyParser({mapParams: true}))

require('./src/routes')(server, getConnection())

server.listen(3000, function () {
  console.log('%s listening at %s', server.name, server.url);
});
CREATE DATABASE moneydb;

USE moneydb;


CREATE TABLE usuario (
	id INT PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(100) NOT NULL, 
	nome VARCHAR(500) NOT NULL,
	id_auth VARCHAR(200) NOT NULL,
	photo_url VARCHAR(500)
);

CREATE TABLE categoria (
	id INT PRIMARY KEY AUTO_INCREMENT,
	descricao VARCHAR(200),
	usuario_id INT NOT NULL
);

ALTER TABLE categoria ADD CONSTRAINT `fk_usuario_categoria` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

CREATE TABLE despesa (
	id INT PRIMARY KEY AUTO_INCREMENT,
	valor DOUBLE, 
	descricao VARCHAR(500),
	data_despesa DATETIME,
	categoria_id INT NOT NULL,
	usuario_id INT NOT NULL
);

ALTER TABLE `despesa` ADD CONSTRAINT `fk_categoria_despesa` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`);
ALTER TABLE `despesa` ADD CONSTRAINT `fk_usuario_despesa` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

CREATE VIEW v_despesa_cliente AS SELECT d.`id`,
	   d.`valor`,
	   d.`descricao`,
	   d.`usuario_id`,
	   d.`data_despesa`,
	   d.`categoria_id`,
	   c.`descricao` AS descricao_categoria 
FROM despesa d, 
	 categoria c, 
	 usuario u 
WHERE c.`usuario_id` = u.id 
	AND d.`usuario_id` = u.`id` 
	AND d.`categoria_id` = c.`id`;

-- DROP TABLE despesa;

-- SHOW VARIABLES LIKE "%version%";

-- INSERT INTO despesa ()